import handleCryptedpass
import subprocess
import time
import re

def lockDatabase():
    handleCryptedpass.encryptOneFile('serveur/data.txt','serveur/data.crypt')
    
    print("Database locked.")
    print("End of data exchange.")

def unlockDatabase():
    handleCryptedpass.decryptOneFile('serveur/data.crypt','serveur/data.txt')
    
    print("database unlocked.")
    print("database ready to use.")


def add(name, numero):
    
    nameE = encryptText(name)
    with open('serveur/data.txt',"r") as f:
        if nameCheck(f.readlines() , nameE) == False:
            f = open('serveur/data.txt',"a")
            f.write( encryptText(name) + " ------ " + encryptText(numero) + '\n')
            f.close()
        else :
            print("Client already exist : checking numero...")
            checkNum(name,numero)

def remove(name):
    nameE = encryptText(name)
    f = open('serveur/data.txt', "r")
    lines = f.readlines()
    f.close()
    nc = nameCheck(lines,nameE)
    if nc == True:
        with open('serveur/data.txt') as myFile:
            for num, line in enumerate(myFile, 1):
                if nameE in line:
                    del lines[num-1]
        new_file = open("serveur/data.txt", "w+")
        for line in lines:
            new_file.write(line)
        new_file.close()
    else:
         print("no name " + name + " found.")

def search(name):
    nameE = encryptText(name)    
    f = open('serveur/data.txt', "r")
    lines = f.readlines()
    f.close()
    nc = nameCheck(lines,nameE)
    if nc == True:
        with open('serveur/data.txt') as myFile:
            for num, line in enumerate(myFile, 1):
                if re.search( "(?:^|\W)"+ nameE + "(?:$|\W)",line):
                    res = lines[num-1].split(" ")[2]
                    print(decryptText(str(res)))
    else:
         print("no name " + name + " found.")                
                          
def checkNum(name,numero):
    
    numeroE = encryptText(numero)
    with open('serveur/data.txt',"r") as f2:
        if nameCheck(f2.readlines() , numeroE) == False:
            print("Client "+ name + " added.")
            f2 = open('serveur/data.txt',"a")
            f2.write(encryptText(name)+ " ------ " + encryptText(numero) + '\n')
        else:
            print("Client already exist.")

def clean(file):
    commande = "rm "+file
    try:
        process = subprocess.Popen(commande,stdout=subprocess.PIPE, shell=True)
        proc_stdout = process.communicate()
        #print(proc_stdout)
        
    except Exception:
        print('error')

def checkingKey(key, vKey1, vKey2):
    handleCryptedpass.decryptOneFile(key,'ramdisk/temp.txt')
    time.sleep(1)
    print(vKey1)
    print(vKey2)
    f1 = open(vKey1,"r")
    f2 = open(vKey2,"r")
    
    with open('ramdisk/temp.txt',"r") as f:
         if str(f1.readline()) == str(f.readline()):
             if str(f2.readline()) == str(f.readline()):
                 return True
    return False

    
def nameCheck(liste,name):
    for i in liste:
        if re.search( "(?:^|\W)"+ name + "(?:$|\W)",i):
            return True
            print("ok")
    return False


def encryptText(text):
    commande = "echo -n " + text + " | openssl enc -base64"
    try:
        process = subprocess.Popen(commande,stdout=subprocess.PIPE, shell=True)
        proc_stdout = process.communicate()[0].strip()
        res = str(proc_stdout).replace("b'" ,"").replace("'","")
        return res
    except Exception:
        print('error')

def decryptText(text):
    commande = "echo " + text.strip() + " | openssl enc -base64 -d"
    try:
        process = subprocess.Popen(commande,stdout=subprocess.PIPE, shell=True)
        proc_stdout = process.communicate()[0].strip()
        res = str(proc_stdout).replace("b'" ,"").replace("'","")
        return res
    except Exception:
        print('error')


