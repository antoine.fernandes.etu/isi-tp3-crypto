import handleDataBase as hdb
import subprocess

import os,glob

def checkPersonal(name):
    f = open('config.py', "r")
    lines = f.readlines()
    #print(lines)
    f.close()
    nc = hdb.nameCheck(lines,name)
    if nc == True:
        with open('config.py') as myFile:
            for num, line in enumerate(myFile, 1):
                temp = line.split(" ")
                if name in temp[2]:
                    return True
                    break
    else:
         print(name + " is not authorized to launch server.")



def plugNewUsb(usbName):
    commande = "mkdir "+ usbName
    print( commande)
    try:
        process = subprocess.Popen(commande,stdout=subprocess.PIPE, shell=True)
        proc_stdout = process.communicate()
        #print(proc_stdout)
    except Exception:
        print('error')

def newPass(path , respName):
    nom = path + "/Clé" + respName +".txt"
    commande = "touch "+ nom
    try:
        process = subprocess.Popen(commande,stdout=subprocess.PIPE, shell=True)
        proc_stdout = process.communicate()
        #print(proc_stdout)
        try:
            f = open(nom,"w+")
            f.write("motdepasseX"+ "\n")
            f.close()
        except Exception:
            print("not good")
    except Exception:
        print('error')

def deleteUsb(usbName):
    commande = "rm -Rf "+ usbName
    try:
        process = subprocess.Popen(commande,stdout=subprocess.PIPE, shell=True)
        proc_stdout = process.communicate()
        #print(proc_stdout)
        print( usbName + " deleted.")
    except Exception:
        print('error')


def checkUsb():
    commande = "ls -ld -- */ | cut -d ' ' -f9"
    try:
        process = subprocess.Popen(commande,stdout=subprocess.PIPE, shell=True)
        proc_stdout = process.communicate()[0].strip()
        res = str(proc_stdout).replace("\\n"," ").replace("/"," ").replace("\'"," ")
        tab = res.split(" ")
        temp_tab = []
        for i in tab:
            if i.startswith("usb"):
                temp_tab.append(i)
        assert(len(temp_tab) ==2)
        return temp_tab
    except Exception:
        print('error can\'t merge')



def répudation(name,usbName, newName, newUsb):
    
    f = open('config.py', "r")
    lines = f.readlines()
    #print(lines)
    f.close()
    cp = checkPersonal(name)
    if cp == True:
        with open('config.py') as myFile:
            for num, line in enumerate(myFile, 1):
                if name in line:
                    del lines[num-1]
                    
        new_file = open("config.py", "w+")
        new_file.write("RESP0 = " + "\"" + newName  + "\""+ "\n")
        for line in lines:
            new_file.write(line)
        new_file.close()
    
    print(name + " repudiated.")
    print("removing "+ name + "\'s usb.") 
    usb_tab = checkUsb()
    for i in usb_tab:
        if i.startswith(usbName):
            deleteUsb(usbName)
    print("plugin new usb...")
    plugNewUsb(newUsb)
    print("Generating new password...")
    newPass(newUsb, newName)
    print("Success.")
     
def getPath(path):

    folder_path = path
    for filename in glob.glob(os.path.join(folder_path, '*.txt')):
      with open(filename, 'r') as f:
        text = f.read()
        return filename