import handleCryptedpass
import handleDataBase
import handleServer as hs
import time

def init_serveur():
    print("Initializing serveur...")
    print(".")
    time.sleep(2)
    print("..")
    time.sleep(2)
    print("...")
    time.sleep(2)
    agent1 = input("Hello, who are you ?")
    if hs.checkPersonal(agent1) == True:
        time.sleep(1)
        print("Validation ok, Welcome "+ agent1)
        clé1 = input("what is your key ?")
        print("usb key plugged in.")
        time.sleep(1)
        agent2 = input("Hello, who are you ?")
        if hs.checkPersonal(agent2) == True:
            time.sleep(1)
            print("Validation ok, Welcome "+ agent2)
            clé2 = input("what is your key ?")
            print("usb key plugged in.")
            time.sleep(0.5)
            print("Generating masterKey...")
            time.sleep(1)
            #création de la clé
            path1 = hs.getPath(clé1)
            path2 = hs.getPath(clé2)
            handleCryptedpass.getEncryptedPass(path1.replace('"',"'"),path2.replace('"',"'"))
            
            print("masterKey generated.")
            print("masterKey check...")
            if handleDataBase.checkingKey('ramdisk/clé.crypt',path1.replace('"',"'"),path2.replace('"',"'")) == True:
                handleDataBase.clean('ramdisk/temp.txt')
                time.sleep(1)
                print("masterKey check successful.")
                print("Opening database...")
                time.sleep(1.5)
                handleDataBase.unlockDatabase()
                time.sleep(1)
                handleDataBase.clean('serveur/data.crypt')
                handleCryptedpass.flush_password_tmp()
                launchDialog()
            else:
                print("masterKey problem.")   
        else:
            print("abort connection.")
    else:
        print("abort connection.")

def shut_down():
    print("Database closing...")
    time.sleep(1.5)
    handleDataBase.lockDatabase()
    time.sleep(1.2)
    handleDataBase.clean('serveur/data.txt')
    handleDataBase.clean('ramdisk/clé.crypt')

def launchDialog():
    while True:
        rep = input("Action : [add/rm/srch/repud/q]")
        if rep == "add":
            name = input("nom : ")
            numero = input("numero : ")
            handleDataBase.add(name,numero)
        if rep == "rm":
            name = input("name : ")
            handleDataBase.remove(name)
        if rep == "srch":
            name = input("name : ")
            handleDataBase.search(name)
        if rep == "repud":
           name = input("name ( to be repudiated ) :")
           usbName = input(name + "\'s usb :")
           newName = input("new name to replace "+name+ " :")
           newUsb = input(newName + "\'s usb :")
           hs.répudation(name,usbName, newName, newUsb)
        if rep == "q":
            shut_down()
            break