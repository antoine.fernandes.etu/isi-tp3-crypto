import subprocess


def encryptOneFile(file, out):
    commande = "openssl enc -aes-256-cbc -in "+ file + " -out "+ out
    try:
        process = subprocess.Popen(commande.split(),
                                   stdin=subprocess.PIPE,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE,
                                   universal_newlines=True,
                                   bufsize=0)
        process.stdin.write("mdp\n")
        process.stdin.write("mdp\n")
        process.stdin.close()
        
        #print(process.stdout)
    except Exception:
        print('error')
    

def decryptOneFile(file, out):
    commande = "openssl enc -d -aes-256-cbc -in "+ file + " -out "+ out
    try:
        process = subprocess.Popen(commande.split(),
                                   stdin=subprocess.PIPE,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE,
                                   universal_newlines=True,
                                   bufsize=0)
        process.stdin.write("mdp\n")
        process.stdin.close()
        
        #print(process.stdout)
    except Exception:
        print('error')
    
def getEncryptedPass(file1, file2):
    
#     file1 = 'usb0/cléResp0.txt'
#     file2 = 'usb1/cléResp1.txt'
    
    merge(file1,file2)
    file3 = 'ramdisk/tmp.txt'
    
    clé = 'ramdisk/clé.crypt'
    command = "openssl enc -aes-256-cbc -in "+ file3 + " -out "+ clé
    
    try:
        process = subprocess.Popen(command.split(),
                                   stdin=subprocess.PIPE,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE,
                                   universal_newlines=True,
                                   bufsize=0)
        process.stdin.write("mdp\n")
        process.stdin.write("mdp\n")
        process.stdin.close()
        
        print("MasterKey activated.")
    except Exception:
        print('error')


def merge(file1, file2):
    commande = "cat "+ file1 + " "+  file2 + " "  + "> ramdisk/tmp.txt" 
    try:
        process = subprocess.Popen(commande,stdout=subprocess.PIPE, shell=True)
        proc_stdout = process.communicate()[0].strip()
        
    except Exception:
        print('error can\'t merge')
    

# openssl enc -aes-256-cbc -in usb0/cléResp0.txt -out clé_0.crypt
# openssl enc -aes-256-cbc -in usb1/cléResp1.txt -out clé_1.crypt
# openssl enc -d -aes-256-cbc -in usb0/clé_0.crypt -out ramdisk/_0_.crypt
# openssl enc -d -aes-256-cbc -in usb1/clé_1.crypt -out ramdisk/_1_.crypt

def flush_password_tmp():
    
    commande = "rm ramdisk/tmp.txt"
    try:
        process = subprocess.Popen(commande,stdout=subprocess.PIPE, shell=True)
        proc_stdout = process.communicate()
        #print(proc_stdout)
        
    except Exception:
        print('error')



     